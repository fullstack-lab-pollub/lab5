Zadanie należało wykonywać w przestrzeni nazw zad5. Zostało to zrealizowane przy pomocy polecenia:


![](/lab5_1.png)


Następnie należało utworzyć plik yaml, który dla przestrzeni nazw zad5 tworzy zestaw ograniczeń na zasoby (quota):


![](/lab5_2.png)


Utowrzono plik yaml, który tworzy Poda w przestrzeni nazw zad5 o nazwie worker:


![](/lab5_3.png)


Bazując na przykładzie application/php-apache.yaml z instrukcji do lab4 i z dokumentacji Kubernetes, zmodyfikowano plik yaml
w następujący sposób, przedstawiony poniżej:


![](/lab5_4.png)


Następnie należało utworzyć plik yaml definiujący obiekt HorizontalPodAutoscaler, który pozwoli
na autoskalowanie wdrożenia (Deployment) php-apache:


![](/lab5_5.png)


Należało również określić wartość maxReplicas tak, aby nie przekroczyć quoty dla przestrzeni nazw zad5.
Dostępne było 2000m zasobów, jeśli chodzi o CPU i 1,5Gi dsotępnej ilości pamięci RAM.
Należy jednak pamiętać, że Pod worker ma ustawione 200Mi pamięci RAM i 200m CPU, patrząc na limits.
Więc trzeba odjąć te wartości od 2000m i 1,5Gi, więc pozostałe wartości zasobów jakie możemy wykorzystać to następująco 1800m i 1,3Gi.
Zatem patrząc na nasz php-apache, który wymaga patrząc na limits 250m CPU i 250Mi pamięci RAM, to maxReplicas może maksymalnie wynosić 5. Ponieważ jeśli podzielimy całkowicie 1800m przez 250m uzyskamy 7, lecz jeśli podzielimy całkowicie 1,3Gi przez 250Mi to otrzymamy 5. Więc podsumowując maxReplicas wynosi w tym przypadku 5.

Poźniej należało utworzyć obiekty zadeklarowane powyżej. Zostało to zrealizowane za pomocą następujących poleceń:


![](/lab5_6.png)
![](/lab5_7.png)


Następnie należało sprawdzić poprawność uruchomienia tych obiektów. Zostało to przedstawione poiżej:


![](/lab5_8.png)
![](/lab5_9.png)
![](/lab5_10.png)
![](/lab5_11.png)
![](/lab5_12.png)
![](/lab5_13.png)
![](/lab5_14.png)


Później należało uruchomić aplikację generującą obciążenie dla aplikacji php-apache. Zrealizowano to za pomocą następującego polecenia:


![](/lab5_16.png)


W powyższym poleceniu wymagane było podanie odpowiedniego url. Aby zobaczyć adres (Endpoints) użyto polecenia:


![](/lab5_15.png)


Następnie, aby zobaczyć czy autoskalowanie działa poprawnie, użyto polecenia:


![](/lab5_17.png)


Po kilku minutach, po wyłączeniu generowania obciążenia ponownie użyto tego samego polecenia:


![](/lab5_18.png)
